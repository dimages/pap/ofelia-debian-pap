FROM php:7.0-fpm

# Platform php dependencies
RUN apt-get update && \
    apt-get install -y libfreetype6-dev libjpeg62-turbo-dev libmcrypt-dev libpng-dev && \
    docker-php-ext-install -j$(nproc) mysqli pdo_mysql mbstring iconv mcrypt && \
    docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ && \
    docker-php-ext-install -j$(nproc) gd

# Install ioncube loader
RUN apt-get update && \
    apt-get install -y wget && \
    wget https://downloads.ioncube.com/loader_downloads/ioncube_loaders_lin_x86-64.tar.gz && \
    tar -zxvf ioncube_loaders_lin_x86-64.tar.gz && \
    mkdir /usr/local/ioncube && \
    cp ioncube/ioncube_loader_lin_7.0.so /usr/local/ioncube/ioncube_loader_lin_7.0.so && \
    echo "zend_extension = /usr/local/ioncube/ioncube_loader_lin_7.0.so" >> /usr/local/etc/php/php.ini && \
    rm ioncube_loaders_lin_x86-64.tar.gz && \
    rm -R ioncube

# Install ofelia
RUN wget https://github.com/mcuadros/ofelia/releases/download/v0.2.2/ofelia_v0.2.2_darwin_amd64.tar.gz && \
    tar -zxvf ofelia_v0.2.2_darwin_amd64.tar.gz && \
    cp ofelia_darwin_amd64/ofelia /usr/local/bin/ofelia && \
    rm ofelia_v0.2.2_darwin_amd64.tar.gz && \
    rm -R ofelia_darwin_amd64

COPY config.ini /etc/ofelia/config.ini
CMD ["ofelia", "daemon", "--config", "/etc/ofelia/config.ini"]